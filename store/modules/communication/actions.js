import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {
    [Constants.DO_QA_LIST]: (store) => {
        return axios.get(apiUrls.DO_QA_LIST)
    },
    [Constants.DO_QA_DETAIL]: (store, payload) => {
        return axios.get(apiUrls.DO_QA_DETAIL.replace('{questionId}', payload.id))
    },
    [Constants.DO_ANSWER_CREATE]: (store, payload) => {
        return axios.post(apiUrls.DO_ANSWER_CREATE.replace('{questionId}', payload.id), payload.data)
    },
}
