const BASE_URL = '/v1/question'

export default {
    DO_QA_LIST: `${BASE_URL}/all`, //get
    DO_QA_DETAIL: `${BASE_URL}/detail/{questionId}`, //단수 get
    DO_ANSWER_CREATE: `/v1/question-answer/new/{questionId}`, //post
}
