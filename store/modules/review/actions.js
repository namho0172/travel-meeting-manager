import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {
    [Constants.DO_REVIEW_LIST]: (store) => {
        return axios.get(apiUrls.DO_REVIEW_LIST)
    },
    [Constants.DO_REVIEW_DETAIL]: (store, payload) => {
        return axios.get(apiUrls.DO_REVIEW_DETAIL.replace('{id}', payload.id))
    },

    [Constants.DO_REVIEW_DELETE]: (store, payload) => {
        return axios.delete(apiUrls.DO_REVIEW_DELETE.replace('{id}', payload.id))
    },
}
