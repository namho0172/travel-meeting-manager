export default {
    DO_REVIEW_LIST: 'review/doList',
    DO_REVIEW_DETAIL: 'review/doDetail',
    DO_REVIEW_DELETE: 'review/doDelete',
}
