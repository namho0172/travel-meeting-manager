const BASE_URL = '/v1/board'

export default {
    DO_REVIEW_LIST: `${BASE_URL}/all`, //get
    DO_DETAIL: `${BASE_URL}/detail/{boardId}`, //get
    DO_REVIEW_DELETE: `${BASE_URL}/del/{boardId}`, //del
}
