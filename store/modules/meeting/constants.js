export default {
    DO_LIST: 'test-data/doList',
    DO_LIST_PAGING: 'test-data/doListPaging',
    DO_DETAIL: 'test-data/doDetail',
    DO_DELETE: 'test-data/doDelete',
    DO_MEETING_CREATE: 'meeting/doCreate'
}
