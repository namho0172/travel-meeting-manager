const BASE_URL = '/v1/packageId'

export default {
    DO_LIST: `${BASE_URL}/all`, //get
    DO_LIST_PAGING: `${BASE_URL}/page/{pageNum}`, //get
    DO_DETAIL: `${BASE_URL}/{id}`, //get
    DO_MEETING_CREATE: `${BASE_URL}/new`, //post
}
