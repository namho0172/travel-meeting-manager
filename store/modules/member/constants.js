export default {
    DO_MEMBER_LIST: 'member/doList',
    DO_MEMBER_DETAIL: 'member/doDetail', //리스트에 있는 회원의 상세정보를 불러오는 DO
    DO_MEMBER_UPDATE: 'member/doUpdate', // 리스트에 있는 회원 정보를 수정하는 DO
    DO_MEMBER_DELETE: 'member/doDelete',
    DO_LIST_PAGING: 'member/doListPaging',
}
