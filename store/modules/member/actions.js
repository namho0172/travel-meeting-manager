import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {
    [Constants.DO_MEMBER_DETAIL]: (store, payload) => {
        return axios.get(apiUrls.DO_MEMBER_DETAIL.replace('{memberId}', payload.id))
    },
    // [Constants.DO_MEMBER_UPDATE]: (store, payload)  => {
    //     console.log(`data : ${payload.data.name}`)
    //     try {
    //         return axios.put(apiUrls.DO_MEMBER_UPDATE.replace('{id}', payload.id), payload.data)
    //     } catch (e) {
    //         console.log(e)
    //     }
    //
    // },
    [Constants.DO_MEMBER_UPDATE]: (store, payload) => {
        return axios.put(apiUrls.DO_MEMBER_UPDATE.replace('{id}', payload.id), payload.data)
    },
    [Constants.DO_MEMBER_DELETE]: (store, payload) => {
        return axios.delete(apiUrls.DO_MEMBER_DELETE.replace('{memberId}', payload.id))
    },
    [Constants.DO_LIST_PAGING]: (store, payload) => {
        return axios.get(apiUrls.DO_LIST_PAGING.replace('{pageNum}', payload.pageNum))
    },
    [Constants.DO_MEMBER_LIST]: (store) => {
        return axios.get(apiUrls.DO_MEMBER_LIST)
    },

}
