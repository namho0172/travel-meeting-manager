const BASE_URL = '/v1/member'

export default {
    DO_MEMBER_LIST: `${BASE_URL}/all`, //get
    DO_MEMBER_DETAIL: `${BASE_URL}/detail/{memberId}`, //단수get
    DO_MEMBER_UPDATE: `${BASE_URL}/memberId/{id}`, //put
    DO_MEMBER_DELETE: `${BASE_URL}/delMember/{memberId}`, //del
    DO_LIST_PAGING: `${BASE_URL}/all/{pageNum}`,
}
